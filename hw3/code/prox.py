import numpy as np
import scipy.io
import matplotlib.pyplot as plt
from matplotlib import rc


def g_func(w, X, y):
    logits = X.dot(w)
    return np.sum(np.log(1 + np.exp(logits)) - y * logits)


def func(w, X, y, groups, l):
    out = g_func(w, X, y)
    for i in range(groups.min(), groups.max() + 1):
        idx = np.where(groups == i)[0]
        out += np.sqrt(idx.shape[0]) * l * np.linalg.norm(w[idx])
    return out


def grad(w, X, y):
    return np.sum(X * (1.0 / (1.0 + np.exp(-X.dot(w))) - y)[:, np.newaxis], axis=0)


def prox(x, t):
    x_norm = np.linalg.norm(x)
    if x_norm < t:
        return 0
    else:
        return (1 - t / x_norm) * x


def prox_step(w, X, y, groups, t, l):
    g_step = w - t * grad(w, X, y)
    p_step = g_step.copy()

    for i in range(groups.min(), groups.max() + 1):
        idx = np.where(groups == i)[0]
        reg = np.sqrt(idx.shape[0]) * l * t
        p_step[idx] = prox(g_step[idx], reg)
    return p_step


def acc_prox_step(w, w_prev, it, X, y, groups, t, l):
    v = w + (it - 2) / (it + 1) * (w - w_prev)
    g_step = v - t * grad(v, X, y)
    p_step = g_step.copy()

    for i in range(groups.min(), groups.max() + 1):
        idx = np.where(groups == i)[0]
        reg = np.sqrt(idx.shape[0]) * l * t
        p_step[idx] = prox(g_step[idx], reg)
    return p_step


if __name__ == '__main__':
	mat = scipy.io.loadmat('data/moviesTest.mat')
	X_test = mat['testRatings'].astype(np.float)
	y_test = mat['testLabels'].squeeze().astype(np.float)
	mat = scipy.io.loadmat('data/moviesTrain.mat')
	X_train = mat['trainRatings'].astype(np.float)
	y_train = mat['trainLabels'].squeeze().astype(np.float)
	mat = scipy.io.loadmat('data/moviesGroups.mat')
	groups = mat['groupLabelsPerRating'].squeeze()

	X_train = np.hstack((X_train, np.ones((X_train.shape[0], 1))))
	X_test = np.hstack((X_test, np.ones((X_test.shape[0], 1))))

	# usual proximal gradient
	t = 1e-4
	l = 5
	epochs = 1000
	fs = np.empty(epochs)
	w = np.zeros(X_train.shape[1])

	for i in range(epochs):
		fs[i] = func(w, X_train, y_train, groups, l)
		w = prox_step(w, X_train, y_train, groups, t, l)
	fs -= 336.207

	# accelerated proximal gradient
	fs_acc = np.empty(epochs)
	w_prev = np.zeros(X_train.shape[1])

	fs_acc[0] = func(w_prev, X_train, y_train, groups, l)
	w = prox_step(w_prev, X_train, y_train, groups, t, l)

	for i in range(1, epochs):
		fs_acc[i] = func(w, X_train, y_train, groups, l)
		w_copy = w.copy()
		w = acc_prox_step(w, w_prev, i + 1, X_train, y_train, groups, t, l)
		w_prev = w_copy
	fs_acc -= 336.207

	rc('text', usetex=True)
	plt.semilogy(np.arange(epochs), fs, label='usual prox grad')
	plt.semilogy(np.arange(epochs), fs_acc, label='accelarated prox grad')
	plt.xlabel("iteration")
	plt.ylabel("$\\log[f(\\beta) - f^*]$")
	plt.legend()
	plt.savefig('plot.pdf')

	y_pred = X_test.dot(w) > 0
	print("Test error: {:.4f}".format(1.0 - np.mean(y_pred == y_test)))

