import cvxpy as cp
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from matplotlib import rc


if __name__ == '__main__':
    train_data = pd.read_csv('data/train.csv')
    X_train = train_data.drop(['label'], axis=1).as_matrix()
    y_train = train_data[['label']].as_matrix()

    test_data = pd.read_csv('data/test.csv')
    X_test = test_data.drop(['label'], axis=1).as_matrix()
    y_test = test_data[['label']].as_matrix()

    w = cp.Variable(X_train.shape[1])
    b = cp.Variable()
    loss = cp.sum_entries(cp.pos(1 - cp.mul_elemwise(y_train, X_train*w + b)))
    reg = cp.norm(w, 2) ** 2
    C = cp.Parameter(sign="positive")
    prob = cp.Problem(cp.Minimize(reg + C*loss))

    C.value = 1
    result = prob.solve()
    w_np = np.array(w.value.tolist())
    b_np = b.value.tolist()
    err_train = np.mean(np.sign(X_train.dot(w_np) + b_np) != y_train)
    err_test = np.mean(np.sign(X_test.dot(w_np) + b_np) != y_test)
    print("Objective:", result)
    print("Train error: {}\nTest error: {}".format(err_train, err_test))

    ks = np.arange(17) - 8
    c_vals = 10 ** (-ks / 2)
    errs_train = np.empty(c_vals.shape[0])
    errs_test = np.empty(c_vals.shape[0])

    for i, c_val in enumerate(c_vals):
        C.value = c_val
        result = prob.solve()
        w_np = np.array(w.value.tolist())
        b_np = b.value.tolist()
        errs_train[i] = np.mean(np.sign(X_train.dot(w_np) + b_np) != y_train)
        errs_test[i] = np.mean(np.sign(X_test.dot(w_np) + b_np) != y_test)

    plt.rc('text', usetex=True)
    font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 16}
    plt.rc('font', **font)
    plt.rc('font', family='serif')

    plt.figure(figsize=(8, 5))
    plt.title("SVM with different C")
    plt.xlabel("$k (C = 10^{-k/2})$")
    plt.ylabel("Error rate")
    plt.plot(ks, errs_train, label='Train data')
    plt.plot(ks, errs_test, label='Test data')
    plt.legend()
    plt.savefig(filename='img.png')

