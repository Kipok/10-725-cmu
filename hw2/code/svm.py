import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from matplotlib import rc
rc('text', usetex=True)


def objective(W, l, X, y):
  C = 10
  m = X.shape[0]
  obj = 0.5 * np.sum(W ** 2)
  dot_mx = X.dot(W.T)
  for j in range(C):
    sign = -(2 * (y == j) - 1.0)
    obj += l / m * np.sum(np.maximum(0.0, 1.0 + sign * dot_mx[:,j]) ** 2)
  return obj


def grad(W, l, X, y):
  C = 10
  m = X.shape[0]
  g = W.copy()

  dot_mx = X.dot(W.T)
  for j in range(C):
    sign = -(2 * (y == j) - 1.0)
    l_func = np.maximum(0.0, 1.0 + sign * dot_mx[:,j])
    g[j] += 2 * l / m * np.sum(sign[:,np.newaxis] * X * l_func[:,np.newaxis], axis=0)
  return g


def accuracy(W, X, y):
  return np.mean(np.argmax(X.dot(W.T), axis=1) == y)


def optimize(W, l, X_train, y_train, num_steps=200, lr=0.005, plot_iter=1, bs=50000):
  objs = []
  train_acc = []
  test_acc = []

  for i in range(num_steps):
    idx = np.random.choice(X_train.shape[0], size=bs, replace=False)
    X = X_train[idx]
    y = y_train[idx]
    g = grad(W, l, X, y)
    W -= lr * g
    if i % plot_iter == 0:
      objs.append(objective(W, l, X_train, y_train))
      train_acc.append(accuracy(W, X_train, y_train))
      test_acc.append(accuracy(W, X_test, y_test))
      print(i, objs[-1], train_acc[-1], test_acc[-1])

  return objs, train_acc, test_acc


def plot_ls(ls, name, **run_params):
  fig, axes = plt.subplots(1, 3, figsize=(17, 3))

  for l in ls:
    print("Lambda = {}".format(l))
    W = np.zeros((10, X_train.shape[1]))
    outs = optimize(W, l, X_train, y_train, **run_params)

    axes[0].plot(np.arange(len(outs[0])), outs[0], label='$\lambda = {}$'.format(l))
    axes[0].set_ylabel('objective')
    axes[0].set_xlabel('epoch')

    axes[1].plot(np.arange(len(outs[1])), outs[1], label='$\lambda = {}$'.format(l))
    axes[1].set_ylabel('train accuracy')
    axes[1].set_xlabel('epoch')

    axes[2].plot(np.arange(len(outs[2])), outs[2], label='$\lambda = {}$'.format(l))
    axes[2].set_ylabel('test accuracy')
    axes[2].set_xlabel('epoch')

  for i in range(3):
    axes[i].legend()
  plt.savefig(name, bbox_inches='tight', pad_inches=0)


if __name__ == '__main__':
  X_train = pd.read_csv('data/train_features.csv', header=None).as_matrix()
  y_train = pd.read_csv('data/train_labels.csv', header=None).as_matrix().squeeze() - 1
  X_test = pd.read_csv('data/test_features.csv', header=None).as_matrix()
  y_test = pd.read_csv('data/test_labels.csv', header=None).as_matrix().squeeze() - 1

  plot_ls([0.1, 0.5, 1.0], 'batch.pdf')
  plot_ls([10.0], 'batch10.pdf')
  plot_ls([0.1, 1.0], 'sgd.pdf', lr=0.001, bs=5000, plot_iter=10, num_steps=2000)
  plot_ls([30.0], 'sgd30.pdf', lr=0.001, bs=5000, plot_iter=10, num_steps=2000)
  plot_ls([50.0], 'sgd50.pdf', lr=0.001, bs=5000, plot_iter=10, num_steps=2000)

