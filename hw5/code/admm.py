import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


def objective(a, A):
    return np.sum(np.maximum(A * a, A * (a - 1)))


def admm_step():
    global p, a, D, B, Z1, Z2, Z3, U1, U2, U3
    y1T = y @ np.ones((1, a.shape[0]))
    B = 0.5 * np.linalg.inv(X.T @ X) @ X.T @ (Z2 - Z1 + U1 - U2 + y1T)

    tmp_x = y1T - X @ B + U1
    Z1 = np.zeros(tmp_x.shape)
    Z1[tmp_x > a / p] = (tmp_x - a / p)[tmp_x > a / p]
    Z1[tmp_x < (a - 1) / p] = (tmp_x - (a - 1) / p)[tmp_x < (a - 1) / p]

    Z2 = (X @ B + U2 + (Z3 - U3) @ D) @ np.linalg.inv(np.eye(a.shape[0]) + D.T @ D)

    Z3 = Z2 @ D.T + U3
    Z3[Z3 < 0] = 0

    U1 = U1 + y1T - X @ B - Z1
    U2 = U2 + X @ B - Z2
    U3 = U3 + Z2 @ D.T - Z3


if __name__ == '__main__':
    X = pd.read_csv('data/X.csv', header=None).as_matrix()
    y = pd.read_csv('data/y.csv', header=None).as_matrix()

    p = 1.0
    num_steps = 50
    a = np.array([0.1, 0.5, 0.9])
    D = np.zeros((a.shape[0] - 1, a.shape[0]))
    D[np.arange(D.shape[0]), np.arange(D.shape[0])] = -1
    D[np.arange(D.shape[0]), np.arange(D.shape[0]) + 1] = 1

    B = np.zeros((X.shape[1], a.shape[0]))
    Z1 = np.zeros((X.shape[0], a.shape[0]))
    Z2 = np.zeros((X.shape[0], a.shape[0]))
    Z3 = np.zeros((X.shape[0], D.shape[0]))

    U1 = np.zeros(Z1.shape)
    U2 = np.zeros(Z2.shape)
    U3 = np.zeros(Z3.shape)

    obj_vals = np.empty(num_steps)
    for i in range(num_steps):
        admm_step()
        obj_vals[i] = objective(a, Z1)
    print('Optimal objective = {:.6f}'.format(obj_vals[-1]))

    fig = plt.figure()
    plt.plot(np.arange(num_steps), obj_vals)
    plt.title('ADMM convergence')
    plt.xlabel('iteration number')
    plt.ylabel('objective values')
    fig.savefig('plot_obj.pdf', bbox_inches='tight')

    fig = plt.figure()
    plt.scatter(X[:,0], y, s=10)
    x_lr = np.array([-1.0, 1.0])
    for i in range(B.shape[1]):
        plt.plot(x_lr, x_lr * B[0, i] + B[1, i], label='alpha = {}'.format(a[i]))
    plt.title('Data quantiles')
    plt.xlabel('x')
    plt.ylabel('y')
    plt.legend()
    fig.savefig('plot_data.pdf', bbox_inches='tight')
