\documentclass[12pt]{article}
\usepackage[margin=1in]{geometry} 
\usepackage{latexsym}
\usepackage{amsmath,amsthm,amssymb,amsfonts}
\usepackage{mathtools}
\usepackage{mathabx}
\usepackage{adjustbox}
\usepackage{bbm}

\newcommand{\N}{\mathbb{N}}
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\E}{\mathbb{E}}
\newcommand{\V}{\mathbb{V}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\Pb}{\mathbb{P}}
\newcommand{\A}{\mathcal{A}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\C}{\mathcal{C}}
\newcommand{\F}{\mathcal{F}}
\newcommand{\Nm}{\mathcal{N}}
\newcommand{\Ll}{\mathcal{L}}

% brackets
\newcommand{\inner}[2]{\left\langle#1,#2 \right\rangle}
\newcommand{\rbr}[1]{\left(#1\right)}
\newcommand{\sbr}[1]{\left[#1\right]}
\newcommand{\cbr}[1]{\left\{#1\right\}}
\newcommand{\nbr}[1]{\left\|#1\right\|}
\newcommand{\abr}[1]{\left|#1\right|}

% operators
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator*{\argmin}{arg\,min}
\DeclareMathOperator*{\argmax}{arg\,max}

\newcommand*\xbar[1]{%
  \hbox{%
    \vbox{%
      \hrule height 0.5pt % The actual bar
      \kern0.5ex%         % Distance between bar and symbol
      \hbox{%
        \kern-0.1em%      % Shortening on the left side
        \ensuremath{#1}%
        \kern-0.1em%      % Shortening on the right side
      }%
    }%
  }%
}%
 
\newenvironment{problem}[2][]{\begin{trivlist}
\item[\hskip \labelsep{\bfseries #1}\hskip \labelsep{\bfseries #2.}]}{\end{trivlist}}

\title{Homework 4}
\author{Igor Gitman, igitman}
\date{}

\setlength\parindent{0pt}
\begin{document}
 
\maketitle

\section{Trust Region}
\begin{problem}{(a)}
    The quadratic approximation of $f(x)$ is
    \[ m(p) = f(x_0) + g^Tp + \frac{1}{2}p^TBp, p = x - x_0 \]
    Since the quadratic approximation of a quadratic function is just another way of rewriting the original function, the unconstrained minimum of
    $m(p)$ is attained at $p = [0, 0] - [1, 1] = [-1, -1]$ (since $[0, 0]$ is the minimum of $f(x)$). Thus, since $\nbr{p}_2 = \sqrt{2} < \Delta = 2$,
    the next step of trust region method will go to the unconstrained minimum of $f(x)$. If $\Delta = \frac{5}{6}$, in order to compute the next step,
    it is necessary to solve the following problem:
    \begin{align*}
        \min_p &\sbr{f(x_0) + g^Tp + \frac{1}{2}p^TBp} \\
        \text{s.t.}\ &\nbr{p}_2 \le \frac{5}{6}
    \end{align*}
    and since the minimum is attained at the boundary in this case it is equivalent to
    \begin{align*}
        \min_p &\sbr{f(x_0) + g^Tp + \frac{1}{2}p^TBp} \\
        \text{s.t.}\ &\frac{1}{2}\sbr{\nbr{p}_2^2 - \frac{25}{36}} = 0
    \end{align*}
    Differentiating the Lagrange function we obtain:
    \begin{align*}
        \frac{\partial L}{\partial p} &= g + Bp + \lambda p = 0 \\
        \frac{\partial L}{\partial \lambda} &= \nbr{p}_2^2 - \frac{25}{36} = 0
    \end{align*}
    Solving for $p$ and $\lambda$ and substituting $g = [1, 2], B = \text{diag}(1, 2)$ we get
    \begin{align*}
        p = -(B + \lambda I)^{-1}g \Rightarrow \nbr{p}_2^2 = \nbr{(B + \lambda I)^{-1}g}_2^2 = \frac{25}{36} 
        \Leftrightarrow \frac{1}{\rbr{1 + \lambda}^2} + \frac{4}{\rbr{2 + \lambda}^2} = \frac{25}{36}
    \end{align*}
    Therefore, $\lambda = 1, p = -[\frac{1}{2}, \frac{2}{3}]$.
\end{problem}
\begin{problem}{(b)}
We know that $p^B = [-1,-1]$ and from the Cauchy point equation we can get that $p^U = \frac{g^Tg}{g^TBg}g = -\frac{5}{9}[1, 2]$. Then, the problem of
finding the next step in the dogleg method splits in 2 parts. First, when $\Delta$ is such so that the steepest descent minimizer is not feasible. 
This means that
\[\frac{5}{9} > \frac{\Delta}{\nbr{g}} \Rightarrow \Delta < \frac{5\sqrt{5}}{9} \]
Then the optimal point is Cauchy point $p = -\frac{\Delta}{\sqrt{5}}[1,2]$. When $\Delta \ge \frac{5\sqrt{5}}{9}$, the optimal point is on the line
connecting Cauchy point and unconstrained minimum. However, since our function is convex, the optimal point will be on the boundary (i.e. as close to
the unconstrained minimum as possible). Thus, it is $p = p^U + \tau(p^B - p^U) $, where $\tau \in [0, 1]$ and can be found from the following equation:
\begin{align*}
    \nbr{p^U + \tau(p^B - p^U)}_2^2 &= \Delta^2 \Leftrightarrow
    \nbr{-\frac{5}{9}\begin{bmatrix} 1 \\ 2 \end{bmatrix} + \tau\rbr{\begin{bmatrix} -1 \\ -1 \end{bmatrix} +\frac{5}{9}\begin{bmatrix} 1 \\ 2
    \end{bmatrix}}}_2^2 = \Delta^2 \Leftrightarrow \\ \Leftrightarrow
    \nbr{\frac{1}{9}\begin{bmatrix} -5 - 4\tau \\ -10 + \tau \end{bmatrix}}_2^2 &= \Delta^2 \Leftrightarrow
    (5+4\tau)^2 + (\tau - 10)^2 = 9^2\Delta^2 \Leftrightarrow \\ \Leftrightarrow 17\tau^2 + 20\tau + 125 - 81\Delta^2 &= 0 \Rightarrow
    \tau = \frac{-10 + \sqrt{1377\Delta^2 - 2025}}{17}
\end{align*}
Since the other root will violate $\tau \in [0,1]$ given that $\Delta > \frac{5\sqrt{5}}{9}$. When $\tau=1$, $\Delta = \sqrt{2}$ and after that the
unconstrained minimum will always be the optimum point. Thus the complete solution is
\[ p = \begin{cases} -\frac{\Delta}{\sqrt{5}}g &\text{if}\ \Delta < \frac{5\sqrt{5}}{9} \\ 
    p^U + \frac{-10 + \sqrt{1377\Delta^2 - 2025}}{17}(p^B - p^U) &\text{if}\ \frac{5\sqrt{5}}{9} \le \Delta < \sqrt{2} \\
    p^B &\text{if}\ \Delta \ge \sqrt{2} \end{cases}
\]
\end{problem}
\newpage
\section{Frank Wolfe}
For all $y, x$ we have:
\begin{align*}
    f(y) - f(x) - \nabla f(x)^T(y - x) \le \frac{\gamma^2}{2}C_f
\end{align*}
Substituting $y=x_{k+1}, x=x_k$ we get
\begin{align*}
    f(x_{k+1}) &\le f(x_k) + \nabla f(x_k)^T(x_{k+1} - x_k) + \frac{\gamma^2}{2}C_f = f(x_k) - \gamma\nabla f(x_k)^T(x_k - s) + \frac{\gamma^2}{2}C_f =
    \\ &= f(x_k) - \gamma g(x_k) + \frac{\gamma_2}{2}C_f
\end{align*}
\newpage
\section{Lagrange Methods and Augmented Lagrangian}
\begin{problem}{(a)}
Without loss of generality, let's assume that $p_1 \ge p_2 \ge \dots \ge p_n$. Then, under the specified assumptions, the algorithm for obtaining the
optimal solution is the following: initialize $Q_c = Q, t = 1, s_i=0\ \forall i=1..n$ and until 
$Q_c = 0$ or $t = n+1$ set $s_t := \min(d_t, Q_c)$, $Q_c := Q_c - s_t$, $t := t+1$. Let's denote with $r$ the first index such that $s_r < d_r$ or
set $r:=n+1$ if it doesn't exist. Obviously, $s_t = 0\ \forall t > r$.

In words, this algorithms is doing the following thing: it decides to send as much as possible to the outlets with highest price until there is no
more product left. Let's prove that this algorithm gives the optimal solution. Let's denote the found solution with $s$ and the total obtained price
with $c = \sum_{i=1}^np_is_i = \sum_{i=1}^rp_is_i$. 
Then, let's look at any other solution $\widetilde{s}$ and it's price $\widetilde{c} = \sum_{i=1}^np_i\widetilde{s}_i$.
If $\widehat{s}_r \le s_r$ or $r > n$ we have:
\begin{align*} 
    c - \widehat{c} &= \sum_{i=1}^rp_i(s_i - \widehat{s}_i) - \sum_{i=r+1}^n\widehat{s}_ip_i \ge p_r\sum_{i=1}^r(s_i - \widehat{s}_i) -
    \sum_{i=r+1}^n\widehat{s}_ip_i = \\ &= p_r(Q - \sum_{i=1}^r\widehat{s}_i) - \sum_{i=r+1}^n\widehat{s}_ip_i = p_r\sum_{i=r+1}^n\widehat{s}_i -
    \sum_{i=r+1}^n\widehat{s}_ip_i = \sum_{i=r+1}^n\widehat{s}_i\sbr{p_r-p_i} \ge 0
\end{align*}
If $\widehat{s}_r > s_r$ then
\begin{align*}
    c - \widehat{c} &= \sum_{i=1}^{r-1}p_i(s_i - \widehat{s}_i) + p_rs_r - \sum_{i=r}^n\widehat{s}_ip_i \ge 
    p_{r-1}\sum_{i=1}^{r-1}(s_i - \widehat{s}_i) + p_rs_r - \sum_{i=r}^n\widehat{s}_ip_i = \\ &= 
    p_{r-1}(Q - s_r - \sum_{i=1}^{r-1}\widehat{s}_i) + p_rs_r - \sum_{i=r}^n\widehat{s}_ip_i = 
    p_rs_r - p_{r-1}s_r + \sum_{i=r}^n\widehat{s}_i\sbr{p_{r-1} - p_i} = \\ &=
    s_r\sbr{p_r - p_{r-1}} + \widehat{s}_r\sbr{p_{r-1} - p_r} + \sum_{i={r+1}}^n\widehat{s}_i\sbr{p_{r-1} - p_i} = \\ &=
    \sbr{\widehat{s}_r - s_r}\sbr{p_{r-1} - p_r} + \sum_{i={r+1}}^n\widehat{s}_i\sbr{p_{r-1} - p_i} \ge 0
\end{align*}
Thus the solution found with the proposed algorithm is optimal since it's value is non-less than the value of any other solution. If we set $y = p_r$
(or $0$ when $r > n$) we automatically prove the first question. When $p_i = y$ we have $0 \le s_r < d_r$.
\end{problem}
\begin{problem}{(b)}
The Lagrange function is:
\[ L(x_1, x_2, \lambda) = 0.5x_1^2 - 0.5x_2^2 - 3x_2 + \lambda x_2 \]
Taking the derivatives with respect to $x_1, x_2, \lambda$ we get $x_1 = x_2 = 0, \lambda = 3$. Minimizing the augmented Lagrangian we get:
\begin{align*} 
    \frac{\partial L_c}{x_1} &= x_1 = 0 \\
    \frac{\partial L_c}{x_2} &= -x_2 - 3 + \lambda + cx = 0 \Rightarrow x_2 = \frac{3 - \lambda}{c - 1}
\end{align*}
Thus for the quadratic penalty method we get:
\begin{align*}
    \lambda &= 0, c = 10 \Rightarrow x = \sbr{0, \frac{3}{9}} = \sbr{0, \frac{1}{3}} \\
    \lambda &= 0, c = 100 \Rightarrow x = \sbr{0, \frac{3}{99}} = \sbr{0, \frac{1}{33}} \\
    \lambda &= 0, c = 1000 \Rightarrow x = \sbr{0, \frac{3}{999}} = \sbr{0, \frac{1}{333}}
\end{align*}
For the method of multipliers we get:
\begin{align*}
    \lambda &= 0, c = 10 \Rightarrow x = \sbr{0, \frac{3}{9}} = \sbr{0, \frac{1}{3}} \\
    \lambda &= \frac{10}{3}, c = 100 \Rightarrow x = \sbr{0, \frac{3 - \frac{10}{3}}{99}} = \sbr{0, -\frac{1}{297}} \\
    \lambda &= \frac{10}{3} - \frac{100}{297}, c = 1000 \Rightarrow x = \sbr{0, \frac{3 - \frac{10}{3} + \frac{100}{297}}{999}} = \sbr{0,
    \frac{1}{296703}}
\end{align*}
If $c$ is constant we have the following update for $\lambda$:
\[ \lambda_{k+1} = \lambda_k + c\frac{3 - \lambda_k}{c - 1} = \frac{3c - \lambda_k}{c - 1} \Rightarrow \abr{\lambda_{k+1} - 3} =
\frac{\abr{\lambda_k - 3}}{\abr{c - 1}} \]
Since in order for $x_2$ to converge to $0$, $\lambda_k$ has to converge to $3$ we have that $c$ has to be greater than $2$. 
\end{problem}
\newpage
\section{Barrier Methods for Support Vector Regression}
\begin{problem}{(a)}
Objective of the barrier method is
\[ f(w, \xi) = t\rbr{\frac{1}{2}\sum_{j=1}^dw_j^2 + C\sum_{i=1}^n\xi_i} - 
   \sum_{i=1}^n\sbr{\log \xi_i + \log\rbr{\xi_i + \epsilon - \abr{Y_i - w^TX_i}}} \]
\end{problem}
\begin{problem}{(b)}
\begin{align*}
    \frac{\partial f}{w_j} &= I(j \le d)tw_j - \sum_{i=1}^n\frac{\text{sign}(Y_i - w^TX_i)X_{ij}}{\xi_i + \epsilon - \abr{Y_i - w^TX_i}} \\ 
    \frac{\partial f}{\xi_i} &= tC - \frac{1}{\xi_i} - \frac{1}{\xi_i + \epsilon - \abr{Y_i - w^TX_i}} \\
    \frac{\partial^2 f}{w_jw_k} &= I(j \le d)I(j = k)t - 
                                 \sum_{i=1}^n \frac{I(Y_i \neq w^TX_i)X_{ij}X_{ik}}{\rbr{\xi_i + \epsilon - \abr{Y_i - w^TX_i}}^2} \\
    \frac{\partial^2 f}{\xi_i\xi_k} &= I(i = k)\sbr{\frac{1}{\xi_i^2} + \frac{1}{\rbr{\xi_i + \epsilon - \abr{Y_i - w^TX_i}}^2}} \\
    \frac{\partial^2 f}{w_j\xi_i} = \frac{\partial^2 f}{\xi_iw_j} &= \frac{\text{sign}(Y_i - w^TX_i)X_{ij}}{\rbr{\xi_i + \epsilon - \abr{Y_i - w^TX_i}}^2} 
\end{align*}
\end{problem}
\begin{problem}{(c)}
    In general, solve 
    \begin{align*}
        \min_{s, x} &\sbr{s} \\
        \text{s.t.}\ &h_i(x) \le s
    \end{align*}
    This can be solved with the same barrier method, since large enough $s$ will be feasible 
    (easy to check directly and increase $s$ until satisfied).

    In the specific case of SVR we can just set $w = 0$ and $\xi$ big enough so that inequalities are 
    satisfied.
\end{problem}
\begin{problem}{(d)}
    Instead of standard newton decrement I used difference in the last 2 objective values as a stopping criteria. Overall, the following results were
    obtained:

    \begin{centering}
        \includegraphics[width=0.49\textwidth]{images/obj.pdf}
        \includegraphics[width=0.49\textwidth]{images/mse.pdf}
    \end{centering}
\end{problem}
\end{document}

