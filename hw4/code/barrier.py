import numpy as np
import matplotlib.pyplot as plt
import pandas as pd


def obj(w_xi, X, y, C):
  n, d = X.shape[0], X.shape[1] - 1
  return 0.5 * np.sum(w_xi[:d] ** 2) + C*np.sum(w_xi[d+1:])


def barrier_obj(w_xi, X, y, C, eps, t):
  n, d = X.shape[0], X.shape[1] - 1
  f = obj(w_xi, X, y, C)
  phi = -np.sum(np.log(w_xi[d+1:]) + np.log(w_xi[d+1:] + eps - np.abs(y - X.dot(w_xi[:d+1]))))
  return f * t + phi


def grad(w_xi, X, y, C, eps, t):
  n, d = X.shape[0], X.shape[1] - 1
  w = w_xi[:d+1]
  xi = w_xi[d+1:]
  g = np.empty(d + 1 + n)
  resid = y - X @ w
  g[:d+1] = -np.sum(X * np.sign(resid)[:,np.newaxis] / (xi[:,np.newaxis] +
                    eps - np.abs(resid)[:,np.newaxis]), axis=0)
  g[:d] += t * w[:d]
  g[d+1:] = t * C - 1.0 / xi - 1.0 / (xi + eps - np.abs(resid))
  return g


def hessian(w_xi, X, y, C, eps, t):
  n, d = X.shape[0], X.shape[1] - 1
  w = w_xi[:d+1]
  xi = w_xi[d+1:]
  H = np.zeros((d + 1 + n, d + 1 + n))
  resid = y - X @ w
  denom = xi + eps - np.abs(resid)
  X_dv = X / (denom[:,np.newaxis])
  H[:d+1,:d+1] = np.einsum('ij,ik->jk', X_dv, X_dv)
  H[:d,:d] += np.eye(d) * t
  H[d+1:,d+1:] = np.eye(n) * (1.0 / xi ** 2 + 1.0 / denom ** 2)
  offdiag = np.sign(resid[:,np.newaxis]) * X / denom[:,np.newaxis] ** 2
  H[:d+1,d+1:] = offdiag.T
  H[d+1:,:d+1] = offdiag
  return H


def check_backtracking(w_xi, v, l, g, X, y, C, eps, t, alpha):
  obj_next = barrier_obj(w_xi - l * v, X, y, C, eps, t)
  obj_cur = barrier_obj(w_xi, X, y, C, eps, t)
  return obj_next <= obj_cur - alpha * l * g.dot(v)


def newton(w_xi_0, X, y, C, eps, t, stop_eps, alpha, beta, max_iter=100):
  w_xi = w_xi_0.copy()
  mses = []
  objs = []
  b_objs = []
  decrs = []
  ls = []
  gs = []
  for i in range(max_iter):
    g = grad(w_xi, X, y, C, eps, t)
    H = hessian(w_xi, X, y, C, eps, t)
    v = np.linalg.solve(H, g)
    l_decr = 0.5 * g.dot(v)
#    if l_decr < stop_eps:
#      break
    if i > 1 and np.abs(b_objs[-1] - b_objs[-2]) <= stop_eps:
      break

    l = 1.0
    while not check_backtracking(w_xi, v, l, g, X, y, C, eps, t, alpha):
      l *= beta
    w_xi -= l * v
    mses.append(np.mean((y - X @ w_xi[:X.shape[1]]) ** 2))
    objs.append(obj(w_xi, X, y, C))
    b_objs.append(barrier_obj(w_xi, X, y, C, eps, t))
    decrs.append(l_decr)
    ls.append(l)
    gs.append(np.linalg.norm(g))

  if i == max_iter - 1:
    print("Stopping epsilon never reached!")
  print("Num iters = {}, decr = {:.10f}".format(i, l_decr))
  return w_xi, mses, objs, (b_objs, decrs, ls, gs)


def barrier_optimizer(w_xi_0, X, y, C, eps, t, mu, m, stop_eps,
                      alpha, beta, max_iter=200):
  w_xi = w_xi_0.copy()
  mses = []
  objs = []
  infos = []
  it_num = 0
  while m / t > stop_eps:
    w_xi, mse_cur, obj_cur, info = newton(w_xi, X, y, C, eps, t, stop_eps, alpha, beta, max_iter)
    mses.append(mse_cur)
    objs.append(obj_cur)
    infos.append(info)
    print("iter #{}: obj={:.4f}, mse={:.4f}".format(it_num, objs[-1][-1], mses[-1][-1]))
    t *= mu
    it_num += 1
  return w_xi, mses, objs, infos


if __name__ == '__main__':
  X = pd.read_csv('data/X.csv').as_matrix()
  y = pd.read_csv('data/y.csv').as_matrix().squeeze()

  X -= X.mean(axis=0)
  X = np.hstack((X, np.ones((X.shape[0], 1))))

  C, eps = 10, 0.1
  m = 2 * X.shape[0]
  alpha, beta = 0.2, 0.9
  stop_eps = 1e-9

  w_xi = np.append(np.zeros(X.shape[1]), 5 * np.ones(X.shape[0]))
  t, mu = 5, 20
  w_xi_opt1, mses1, objs1, infos1 = barrier_optimizer(w_xi, X, y, C, eps, t, mu, m, stop_eps, alpha, beta)
  t, mu = 10000, 5
  w_xi_opt2, mses2, objs2, infos2 = barrier_optimizer(w_xi, X, y, C, eps, t, mu, m, stop_eps, alpha, beta)

  mses1_unrolled = np.array([m for mse in mses1 for m in mse])
  mses2_unrolled = np.array([m for mse in mses2 for m in mse])
  plt.figure()
  plt.title("MSE")
  plt.semilogy(np.arange(len(mses1_unrolled)), mses1_unrolled - 0.365, label="t=5, mu=20")
  plt.semilogy(np.arange(len(mses2_unrolled)), mses2_unrolled - 0.365, label="t=10000, mu=5")
  plt.legend()
  plt.savefig('mse.pdf')

  objs1_unrolled = np.array([o for objc in objs1 for o in objc])
  objs2_unrolled = np.array([o for objc in objs2 for o in objc])
  plt.figure()
  plt.title("Objective")
  plt.semilogy(np.arange(len(objs1_unrolled)), objs1_unrolled - 729.652, label="t=5, mu=20")
  plt.semilogy(np.arange(len(objs2_unrolled)), objs2_unrolled - 729.652, label="t=10000, mu=5")
  plt.legend()
  plt.savefig('obj.pdf')

